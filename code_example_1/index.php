

<?php

require_once 'app/init.php';



if(!empty($_POST))
{
	
	$email = $_POST['email'];
	$username = $_POST['username'];
	$password = $_POST['password'];
  $firstname = $_POST['firstname'];
  $lastname = $_POST['lastname'];
  $profile_img = 'default.jpg';

	$validator = new Validator($database, $errorHandler);

	$validation = $validator->check($_POST, [
		'email' => [
			'required' => true,
			'maxlength' => 200,
			'unique' => 'users',
			'email' => true
		],
		'username' => [
			'required' => true,
			'minlength' => 3,
			'maxlength' => 20,
			'unique' => 'users'
		],
		'password' => [
			'required' => true,
			'minlength' => 5
		]
	]);

	if($validation->fails())
	{
		echo '<pre>', print_r($validation->errors()->all(), true), '</pre>';
	}
	else
	{
		$created = $auth->create([
			'email' => $email,
			'username' => $username,
			'password' => $password,
      'first_name' => $firstname,
      'last_name' => $lastname,
      'profile_img' => $profile_img
		]);

		if($created)
		{
			echo "<p>Account created</p>";
      header('Location: fareshare.php');
		}
	}
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />

    <title>FareShare</title>

    <!-- Bootstrap core CSS -->
     <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/main.css" rel="stylesheet">


    <!--http://getbootstrap.com/customize/?id=e54274761290ce6b7e21-->

   

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

     <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Main Nav

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

 

        <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="helper" href="#">
        <img alt="Brand" src="img/fs_logo.png" style="width:260px">
      </a>
      <ul class="main-menu">
       <form class="form-inline login-form" action="signin.php" method="post">
       	<input type="text" class="form-control" name="username" placeholder="Username" >
       	<input type="password" class="form-control" name="password" placeholder="Password" >
       	<button type="submit" class="btn btn-default">Sign In</button>
       </form>
      </ul>
    
    </div>
  </div>
</nav>

<div class="ghost">
</div>


    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Journey Section

         
         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    <div class="road-bg">
      <noscript><p class="bg-danger" style="color:#E03C48; text-align:center; margin-top:0; padding-bottom:15px; top:-10px; font-size:20px;">
 This application needs javascript to function properly. Please turn it on in your browser.
</p></noscript>

      
       <div class="row">

        <div class="col-sm-6">
        <h1>Share the road</h1>
        <h2>For less...</h2>
        </div><!--clo-sm-6-->
       
        <div class="col-sm-6">
         <div class="panel-head" style="opacity:0.9;"><h1>Sign up for free</h1></div><!--panel head-->

    
   
        <div class="panel-body" style="opacity:0.9;">

  <form class="form-horizontal" method="post" action="index.php" >
  <br />
   <div class="form-group">
    <div class="col-sm-1">
     
     </div>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="username" placeholder="Username">
    </div>
     <div class="col-sm-1">
     
     </div>
  </div>

    <div class="form-group">
    <div class="col-sm-1">
     
     </div>
    <div class="col-sm-10">
      <input type="email" class="form-control" name="email" placeholder="Email">
    </div>
     <div class="col-sm-1">
     
     </div>
  </div>

    <div class="form-group">
    <div class="col-sm-1">
     
     </div>
    <div class="col-sm-10">
      <input type="text" class="form-control" pattern="[A-Za-z]{50}" title="Letters only" name="firstname" placeholder="First Name">
    </div>
     <div class="col-sm-1">
     
     </div>
  </div>
  

    <div class="form-group">
    <div class="col-sm-1">
     
     </div>
    <div class="col-sm-10">
      <input type="text" class="form-control" pattern="[A-Za-z]{50}" title="Letters only" name="lastname" placeholder="Last Name">
    </div>
     <div class="col-sm-1">
     
     </div>
  </div>
  

    <div class="form-group">
    <div class="col-sm-1">
     
     </div>
    <div class="col-sm-10">
      <input type="password" class="form-control" pattern=".{6,}" title="Must be at least 6 characters" name="password" placeholder="Password">
    </div>
     <div class="col-sm-1">
     
     </div>
  </div>
  
    <div class="form-group">
    <div class="col-sm-1">
     
     </div>
    <div class="col-sm-10">
      <input type="password" class="form-control" pattern=".{6,}" name="confirm_password" placeholder="Confirm Password">
    </div>
     <div class="col-sm-1">
     
     </div>
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Sign up</button>
    </div>
  </div>
</form>
         </div><!--panel body-->
        </div><!--clo-sm-6-->
      </div><!--row-->

    


      <div class="row">

        <div class="col-sm-12">
    
       </div><!--clo-sm-12-->
      </div><!--row-->



    </div><!--road bg-->
    

  
 
   


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
      <script src="js/main.js"></script>
    
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

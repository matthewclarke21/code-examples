var directionsDisplay; //new
var directionsService = new google.maps.DirectionsService(); //new
var map;
var geocoder;
var bounds = new google.maps.LatLngBounds();
var obj;




var origin1;   //start point
var destinationA; // End point


var destinationIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=D|FF0000|000000';
var originIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000';



// Create the map
function initialize() {
  directionsDisplay = new google.maps.DirectionsRenderer(); //new
  var opts = {
    center: new google.maps.LatLng(55.378051, -3.435973),
    zoom: 5,
    scrollwheel: false,
    styles: [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#373c47"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            },
            {
                "color": "#c2c4c7"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#150404"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#373c47"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#c2c4c7"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#373c47"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#c4e6ea"
            },
            {
                "visibility": "on"
            }
        ]
    }
]

  };
  map = new google.maps.Map(document.getElementById('map-canvas'), opts);
  directionsDisplay.setMap(map); //new
  geocoder = new google.maps.Geocoder();

}


//calcuate distance
function calculateDistances() {
  


var origin1 = $('#origin1').val();
var destinationA = $('#destinationa').val();

var request = {
      origin: origin1,
      destination: destinationA,
      travelMode: google.maps.TravelMode.DRIVING
  }; //new

  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  
  

  }); //new

  

  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix(
    {
      origins: [origin1],
      destinations: [destinationA],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.IMPERIAL,
      avoidHighways: false,
      avoidTolls: false
    }, callback);
}


//Output result
function callback(response, status) {
  if (status != google.maps.DistanceMatrixStatus.OK) {
    alert('Error was: ' + status);
  } else {
    var origins = response.originAddresses;
    var destinations = response.destinationAddresses;
   
   
    //deleteOverlays();

    for (var i = 0; i < origins.length; i++) {
      var results = response.rows[i].elements;
     
      for (var j = 0; j < results.length; j++) {
        
        
        //values retrieved from hidden form
        var mpg = $('.mpg').val();
        var passengers = $('.passengers').val();
        var fuelcost = $('.fuel_cost').val();
        //store distance of the journey
        var miledDistance = parseFloat(results[j].distance.text);
      
        //if the user selected return journey double the distance
        var singlereturn = $('.single-return .selected').text();
        if(singlereturn == 'Return'){
            miledDistance = miledDistance * 2; 
        }
        //Convert the MPG into MPL 
        var mpl = mpg / 4.54609188;
        // work our how many litres where used for the journey
        var fuelused = miledDistance / mpl;
        // work out cost per person in the format £xx.xx
        var costPerPerson = ((fuelcost/100) * fuelused) / passengers;
        
   
var obj= {
"Overview" : [
    {
        "Distance": miledDistance,
        "CostPerPerson": costPerPerson.toFixed(2),
        "Time": results[j].duration.text 
        
    },

  ]
};


var templateSource   = $("#journey_overview_tmpl").html();
template = Handlebars.compile(templateSource);
data = template(obj);
$("#outputDiv2").empty();
$("#outputDiv2").append(data);
$('#distance').val(miledDistance);
$('#distance').val(miledDistance);
$('#cost').val(costPerPerson.toFixed(2));

$('#map-overlay').fadeIn();

    
/*

     obj= {
"Overview" : [
    {
        "Distance": +miledDistance,
        "CostPerPerson": +costPerPerson,
        "Time": +results[j].duration.text
        
    },

  ]
};
*/
 



  



//'Start Point: ' + origins[i] + '<br /><br />' + 'End Point: ' + destinations[j]




   /*     outputDiv.innerHTML += 
            + '<br /><br />Distance: ' + miledistance + ' Miles' + '<br /><br />Time: '
            + results[j].duration.text + '<br><br>' + passengers + ' Passenger/s x &pound;' + (costPerPerson).toFixed(2);;
      
      */

      }
    }
  }
}

google.maps.event.addDomListener(window, 'load', initialize);
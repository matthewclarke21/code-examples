$(document).ready(function () {
  $('#loading').hide();
  journey_validate();

});
////////////////////////////////////////////////////////////////////////////// Main Menu and revealing sections

$(".main-menu li").click(function (e) {

  e.preventDefault();

  //add active class to the select menu icon and remove from others
  $(this).addClass("active");
  $(this).siblings().removeClass("active");

  //Get the id value from the menu which matches each section
  var section = $(this).attr('id');

  //run function to load friends if the friend section is clicked
  if (section == '.friends-section') {
    loadFriends();
  }

  if (section == '.settings-section') {
    loadsettings();
    loadFuel();
  }

  if (section == '.payment-section') {
    loadPayment();
  }
  //Hide all sections and display the one that matches the variable 'section'
  $('.journey-section, .friends-section, .payment-section, .settings-section').hide();
  $(section).show();
  //$(section).addClass('animated zoomIn');


  //Turn the on state for the selected link
  $(this).find('img').attr('src', $(this).find('img').attr('src').replace("_off", "_on"));

  //for each sibling it changes the image to the off state
  $.each($(this).siblings(), function () {
    $(this).find('img').attr('src', $(this).find('img').attr('src').replace("_on", "_off"));



  });


});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//  Friends Section


//////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////// Load friends


function loadFriends() {

  //clear previous results
  $("#friends_list").empty();


  $.ajax({
    url: "app/allfriends.php",
    type: "POST",
    dataType: "json",
    success: showFriends,
    error: errorFriends
  });


  function showFriends(responseData) {
    //Use handlebars templating to display the results
    var source = $("#friend_tmpl").html();
    var template = Handlebars.compile(source);
    $("#friends_list").append(template({
      objects: responseData
    }));
  }

  function errorFriends() {
    $("#friends_list").append('<br /><br /><p style="font-size:15px;">Unable to load friends. Please try refreshing the page.</p>')
  }


}


////////////////////////////////////////////////////////////////////////////// Search for friend


$("#find_friend").on("click", function () {


  $("#find_result").empty();

  var search_username = $('#search_friend').val();






  $.ajax({
    url: "app/allfriends.php",
    type: "POST",
    data: {
      search_username: search_username
    },
    dataType: 'json',
    success: addFriend,
    error: errorFriend2
  });



  function addFriend(responseData) {


    var source = $("#search_tmpl").html();
    var template = Handlebars.compile(source);
    $("#find_result").append(template({
      objects: responseData
    }));
    var query = $("#find_result .img-circle").attr("src");


    $("#find_result").css('display', 'block');

  }

  function errorFriend2() {
    alert("can't load");

  }

});

//////////////////////////////////////////////////////////////////////// Accept, Decline, Delete or block friends

$(".panel-body").on("click", '.dropdown-menu li', function (e) {

  e.preventDefault();
  var value = $(this).val();
  var type = $(this).text();




  $.ajax({
    url: "app/allfriends.php",
    type: "POST",
    data: {
      value: value,
      type: type
    },
    success: loadFriends,
    error: errorFriend2

  });



  function errorFriend2() {
    alert("can't load");

  }


});




/////////////////////////////////////////////////////////////////////////// add Friend

$(document).ready(function () {
  $("#find_result").on("click", ".add_friend", function () {

    friendID = $(this).val();
    alert(friendID);


    $.ajax({
      url: "app/allfriends.php",
      type: "POST",
      data: {
        friendID: friendID
      },
      success: addFriend,
      error: failFriend
    });



    function addFriend() {

      loadFriends();
      $('.add_friend').empty().text('Pending');
      $(".add_friend").prop('disabled', true);

    }

    function failFriend() {
      alert("error");

    }



  });
});



//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//  Journey Section


//////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////// Driver Modal

$("#add_driver").on("click", function () {

  //animate steering wheel
  $(this).find('img').addClass('animated swing');

  setTimeout(function () {
    $('#add_driver').find('img').removeClass('animated swing');
  }, 1000);


  $("#driver_list").empty();

  var get_drivers = 1;

  $.ajax({
    url: "app/journey.php",
    type: "POST",
    dataType: "json",
    data: {
      get_drivers: get_drivers
    },
    success: showDriver,
    error: errorDriver
  });


  function showDriver(responseData) {
    //Use handlebars templating to display the results
    var source = $("#driver_tmpl").html();
    var template = Handlebars.compile(source);
    $("#driver_list").append(template({
      objects: responseData
    }));

  }

  function errorDriver() {
    $("#driver_list").append('<br /><br /><p style="font-size:15px;">Unable to load friends who are drivers.</p>')
  }





});

////////////////////////////////////////////////////////////////////////////// add MPG and Fuel cost to invisble form

$("#driver_list").on("click", '.driver_id', function () {

  $('#reset_car').find('img').removeClass('animated rotateIn');
  //Get the div title
  var driver_id = $(this).attr("title");

  $('.journey-driver').empty();
  $(this).clone().children().appendTo('.journey-driver');

  $.ajax({
    url: "app/journey.php",
    type: "POST",
    dataType: "json",
    data: {
      driver_id: driver_id
    },
    success: success,
    error: error
  });

  function success(responseData) {

    //Add driver's ID to the hidden form
    $('#driver').val(driver_id);
    journey_validate();

    var source = $("#fuel_tmpl").html();
    var template = Handlebars.compile(source);
    $("#hidden_mpg_fuel").append(template({
      objects: responseData
    }));


  }


  function error() {
    alert('dont work');
  }




});


////////////////////////////////////////////////////////////////////////////// Passenger Modal

$("#add_passenger").on("click", function () {


  $("#passenger_add_list").empty();
  var get_passengers = 1;

  $.ajax({
    url: "app/journey.php",
    type: "POST",
    dataType: "json",
    data: {
      get_passengers: get_passengers
    },
    success: showPassenger,
    error: errorPassenger
  });


  function showPassenger(responseData) {
    //Use handlebars templating to display the results
    var source = $("#passenger_add_tmpl").html();
    var template = Handlebars.compile(source);
    $("#passenger_add_list").append(template({
      objects: responseData
    }));


  }

  function errorPassenger() {
    $("#passenger_add_list").append('<br /><br /><p style="font-size:15px;">Unable to load friends.</p>')
  }





});

/////////////////////////////////////////////////////////////////////////// Add to display passengers

$("#passenger_add_list").on("click", '.passenger_id', function () {

  var driver = $('#driver').val();
  var p1 = $('#p1').val();
  var p2 = $('#p2').val();
  var p3 = $('#p3').val();
  var p4 = $('#p4').val();



  //Get the div title
  var passenger_id = $(this).attr("title");

  //$('.journey-passengers').empty();

  if (driver == passenger_id) {
    alert('this person is the driver');
  } else if (p1 != passenger_id && p2 != passenger_id && p3 != passenger_id && p4 != passenger_id) {
    $(this).clone().appendTo('.passenger-footer');
    $(this).clone().appendTo('.journey-passengers');


    if (p1 == '') {
      $('#p1').val(passenger_id);
      journey_validate();
      $('#reset_car').find('img').removeClass('animated rotateIn');
      $('.passengers').val('2');
    } else if (p2 == '') {
      $('#p2').val(passenger_id);
      $('.passengers').val('3');
    } else if (p3 == '') {
      $('#p3').val(passenger_id);
      $('.passengers').val('4');
    } else if (p4 == '') {
      $('#p4').val(passenger_id);
      $('.passengers').val('5');
    }


  }





});

/////////////////////////////////////////////////////////////////////////// Remove display passengers



$(".passenger-footer").on("click", '.passenger_id', function () {


  var driver = $('#driver').val();
  var p1 = $('#p1').val();
  var p2 = $('#p2').val();
  var p3 = $('#p3').val();
  var p4 = $('#p4').val();

  //Get the div title
  var passenger_id = $(this).attr("title");

  if (p1 == passenger_id) {
    $('#p1').val('');

  } else if (p2 == passenger_id) {
    $('#p2').val('');
  } else if (p3 == passenger_id) {
    $('#p3').val('');
  } else if (p4 == passenger_id) {
    $('#p4').val('');

  }

  //passenger number fix
  if (p1 == '' && p2 == '' && p3 == '' && p4 == '') {
    $('.passengers').val('0');
  }

  if ((p1 != '' && p2 != '' && p3 == '' && p4 == '') || (p1 != '' && p3 != '' && p2 == '' && p4 == '') || (p1 != '' && p4 != '' && p2 == '' && p3 == '') ||
    (p1 == '' && p3 != '' && p2 != '' && p4 == '') || (p1 == '' && p4 != '' && p2 != '' && p3 == '') ||
    (p1 == '' && p3 == '' && p3 != '' && p4 != '')) {

    $('.passengers').val('2');

  }

  if ((p1 != '' && p2 != '' && p3 != '' && p4 == '') || (p1 != '' && p2 != '' && p3 == '' && p4 != '') || (p1 != '' && p2 == '' && p3 != '' && p4 != '')) {

    $('.passengers').val('3');

  }





  $(this).empty();
  $(this).appendTo('.journey-passengers');
  $('.journey-passengers div[title="' + passenger_id + '"]').remove();
  journey_validate();






});

/////////////////////////////////////////////////////////////////////////// Reset who is going section
$("#reset_car").on("click", function () {



  //clear form values
  $('#driver').val('');
  $('#p1').val('');
  $('#p2').val('');
  $('#p3').val('');
  $('#p4').val('');
  $('.passengers').val('');

  //
  $("#map-overlay").fadeOut();

  //clear 'who is going' section
  $('.journey-passengers').empty();
  $('.journey-driver').empty();
  $('.passenger-footer').empty();
  $("#hidden_mpg_fuel").empty();


  journey_validate();

  //add classes for animation to run
  $(this).find('img').addClass('animated rotateIn');


});

////////////////////////////////////////////////////////////////////////////// single/return button

$('.single-return button').on("click", function () {

  $(this).siblings().removeClass('selected').addClass('btn-default');
  $(this).addClass('selected').removeClass('btn-default');

});

////////////////////////////////////////////////////////////////////////////// enable and disable calculate button

function journey_validate() {

  var driver = $('#driver').val();
  var p1 = $('#p1').val();
  var origin = $('#origin1').val();
  var destination = $('#destinationa').val();


  if (driver != '' && p1 != '') {
    $('.view_map').prop("disabled", false);
  } else {
    $('.view_map').prop("disabled", true);
  }



}

////////////////////////////////////////////////////////////////////////////// Scroll to map on click

/*   NOT WORKING */

$(".view_map").on("click", function () {

  $('html, body').animate({
    scrollTop: $("#map-overlay").offset().top
  }, 2000);



});

////////////////////////////////////////////////////////////////////////////// Submit journey to database

$(".submit_journey").on("click", function () {


  var driver = $('#driver').val();
  var p1 = $('#p1').val();
  var p2 = $('#p2').val();
  var p3 = $('#p3').val();
  var p4 = $('#p4').val();
  var distance = $('#distance').val();
  var cost = $('#cost').val();
  var sendJourney = 1;


  $.ajax({
    url: "app/journey.php",
    type: "POST",
    data: {
      sendJourney: sendJourney,
      driver: driver,
      p1: p1,
      p2: p2,
      p3: p3,
      p4: p4,
      distance: distance,
      cost: cost
    },
    success: journeySuccess,
    error: journeyFail
  });


  function journeySuccess() {
    alert('Journey Submited');
  }

  function journeyFail() {
    alert('Journey Failed');
  }




});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//  Payment Section


//////////////////////////////////////////////////////////////////////////////////////////////////////////////

function loadPayment() {
  verify_journeys();
  payment_overview();
  whatYouAreOwed();
  whatYouOwe();
}

function verify_journeys() {
  var verify_journeys = 1;

  $.ajax({
    url: "app/payment.php",
    type: "POST",
    dataType: "json",
    data: {
      verify_journeys: verify_journeys
    },
    success: showVerifyJourney,
    error: errorVerify
  });


  function showVerifyJourney(responseData) {
    //Use handlebars templating to display the results
    var source = $("#verifyJourney_tmpl").html();
    var template = Handlebars.compile(source);
    $("#verifyJourney-input").empty();
    $("#verifyJourney-input").append(template({
      objects: responseData
    }));
  }

  function errorVerify() {
    $("#payment-input").append('<br /><br /><p style="font-size:15px;">Unable to load Journey Verifications. Please try refreshing the page.</p>')
  }

}




function payment_overview() {
  var payment_overview = 1;

  $.ajax({
    url: "app/payment.php",
    type: "POST",
    dataType: "json",
    data: {
      payment_overview: payment_overview
    },
    success: showPaymentOverview,
    error: errorPaymentOverview
  });

  function showPaymentOverview(responseData) {
    //Use handlebars templating to display the results
    var source = $("#paymentOverview_tmpl").html();
    var template = Handlebars.compile(source);
    $(".payment_overview").empty();
    $(".payment_overview").append(template({
      objects: responseData
    }));
  }

  function errorPaymentOverview() {
    $(".payment_overview").append('<br /><br /><p style="font-size:15px;">Unable to load payment overview. Please try refreshing the page.</p>')
  }



}

function whatYouAreOwed() {
  var whatYouAreOwed = 1;

  $.ajax({
    url: "app/payment.php",
    type: "POST",
    dataType: "json",
    data: {
      whatYouAreOwed: whatYouAreOwed
    },
    success: showWhatYouAreOwed,
    error: errorShowWhatYouAreOwed
  });

  function showWhatYouAreOwed(responseData) {
    //Use handlebars templating to display the results
    var source = $("#whatYouAreOwed_tmpl").html();
    var template = Handlebars.compile(source);
    $(".whatYouAreOwed").empty();
    $(".whatYouAreOwed").append(template({
      objects: responseData
    }));
  }

  function errorShowWhatYouAreOwed() {
    $(".whatYouAreOwed").append('<br /><br /><p style="font-size:15px;">Unable to load what you are owed. Please try refreshing the page.</p>')
  }


}



var journey_id;
$(".whatYouAreOwed").on("click", ".whatYouAreOwed_verify", function () {

  journey_id = $(this).attr("title");

  $(".verifyText_input").empty();
  $(this).parent().find('span').clone().appendTo(".verifyText_input");

});



$(".whatYouAreOwed2").on("click", function () {



  $.ajax({
    url: "app/payment.php",
    type: "POST",

    data: {
      journey_id: journey_id
    },
    success: verifyPayment,
    error: errorVerifyPayment
  });

  function verifyPayment() {
    loadPayment();
    $('#paymentVerifyModal').modal('hide');
  }


  function errorVerifyPayment() {
    alert('unable to confirm');
    $('#paymentVerifyModal').modal('hide');
    loadPayment();
  }

});





function whatYouOwe() {
  var whatYouOwe = 1;

  $.ajax({
    url: "app/payment.php",
    type: "POST",
    dataType: "json",
    data: {
      whatYouOwe: whatYouOwe
    },
    success: showWhatYouOwe,
    error: errorShowWhatYouOwe
  });

  function showWhatYouOwe(responseData) {
    //Use handlebars templating to display the results
    var source = $("#whatYouOwe_tmpl").html();
    var template = Handlebars.compile(source);
    $(".whatYouOwe").empty();
    $(".whatYouOwe").append(template({
      objects: responseData
    }));
  }

  function errorShowWhatYouOwe() {
    $(".whatYouOwe").append('<br /><br /><p style="font-size:15px;">Unable to load who you owe. Please try refreshing the page.</p>')
  }


}


$("#payment_history").on("click", function () {

  var payment_history = 1;

  $.ajax({
    url: "app/payment.php",
    type: "POST",
    dataType: "json",
    data: {
      payment_history: payment_history
    },
    success: showPaymentHistory,
    error: errorPaymentHistory
  });

  function showPaymentHistory(responseData) {
    //Use handlebars templating to display the results
    var source = $("#paymentHistory_tmpl").html();
    var template = Handlebars.compile(source);
    $("#paymentHistoryInfo").empty();
    $("#paymentHistoryInfo").append(template({
      objects: responseData
    }));
  }

  function errorPaymentHistory() {
    $("#paymentHistoryInfo").empty();
    $("#paymentHistoryInfo").append('<br /><br /><p style="font-size:15px;">Unable to load payment history. Please try refreshing the page.</p>')
  }






});



//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//  Settings Section


//////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadsettings() {

  //clear previous results
  $("#settings-input").empty();


  $.ajax({
    url: "app/settings.php",
    type: "POST",
    dataType: "json",
    success: showSettings,
    error: error
  });


  function showSettings(responseData) {
    //Use handlebars templating to display the results
    var source = $("#settings_tmpl").html();
    var template = Handlebars.compile(source);
    $("#settings-input").append(template({
      objects: responseData
    }));
  }

  function error() {
    $("#settings-input").append('<br /><br /><p style="font-size:15px;">Unable to load Settings. Please try refreshing the page.</p>')
  }


}



$("#settings-input").on("click", ".update-settings", function (e) {
  e.preventDefault();




  var email = $(this).prev().find('input').val();
  var last_name = $(this).prev().prev().find('input').val();
  var first_name = $(this).prev().prev().prev().find('input').val();
  var username = $(this).prev().prev().prev().prev().find('input').val();

  alert(email);


  $.ajax({
    url: "app/settings.php",
    type: "POST",
    data: {
      username: username,
      first_name: first_name,
      last_name: last_name,
      email: email
    },
    success: updateSettings,
    error: error
  });



  function updateSettings() {
    loadsettings();
  }

  function error() {
    $("#settings-input").append('<br /><br /><p style="font-size:15px;">Unable to load Settings. Please try refreshing the page.</p>')
  }




});

////////////////////////////////////////////////////////////////////////////// load fuel prices function

function loadFuel() {


  $.ajax({
    url: "app/fuelFeed.php",
    type: "POST",
    dataType: 'json',
    success: updateFuelPrice,
    error: error
  });


  function updateFuelPrice(responseData) {
    //Use handlebars templating to display the results
    $("#fuel_radio").empty();
    var source = $("#fuelPrices_tmpl").html();
    var template = Handlebars.compile(source);
    $("#fuel_radio").append(template({
      objects: responseData
    }));
  }

  function error() {
    $("#fuel_radio").append('<br /><br /><p style="font-size:15px;">Unable to Fuel Settings</p>')
  }




}


////////////////////////////////////////////////////////////////////////////// Update pence per litre box

$('#fuel_radio').on('change', 'input', function () {

  var selected_price = $(this).val();
  $('#fuel_price').val(selected_price);

  if (selected_price != '') {
    $('#fuel_price').prop('readonly', true);
  } else {
    $('#fuel_price').prop('readonly', false);
  }

});

////////////////////////////////////////////////////////////////////////////// Update car info in database

$('#fuel_radio').on('click', '.update-car', function () {

  var car_update = 1;
  var fuel_cost = $(this).parent().find('#fuel_price').val();
  var mpg = $('#mpg_input').val();


  $.ajax({
    url: "app/settings.php",
    type: "POST",
    data: {
      car_update: car_update,
      mpg: mpg,
      fuel_cost: fuel_cost
    },
    success: updateFuelPrice,
    error: error
  });


  function updateFuelPrice(responseData) {
    alert('updated');
  }

  function error() {
    alert('didnt update');
  }




});


////////////////////////////////////////////////////////////////////////////// Activate/deactivate upload button

$('#fileToUpload').on('change', function () {

  var file = $('#fileToUpload').val();
  alert(file);
  if (file != '') {
    $('#upload_btn').prop("disabled", false);
  } else {
    $('#upload_btn').prop("disabled", true);
  }



})




////////////////////////////////////////////////////////////////////////////// animate main menu

$('.main-menu').on('mouseenter', 'li', function () {

  $(this).addClass('animated pulse');

}).on('mouseleave', 'li', function () {

  $(this).removeClass('animated pulse');


});


////////////////////////////////////////////////////////////////////////////// animate brand logo

$('#brand').on('click', function () {

  $(this).addClass('animated tada');

  $("html, body").animate({
    scrollTop: 0
  }, 600);
  return false;

});






////////////////////////////////////////////////////////////////////////////// Ajax loading
var $loader = $('#loading'),
  timer;

$loader.hide()
  .ajaxStart(function () {
    timer && clearTimeout(timer);
    timer = setTimeout(function () {
        $loader.fadeIn('100');
      },
      300);
  })
  .ajaxStop(function () {
    clearTimeout(timer);
    $loader.fadeOut('100');
  });
<?php

require_once 'app/init.php';

if(!empty($_POST))
{
	$username = $_POST['username'];
	$password = $_POST['password'];

	$signin = $auth->signin([
		'username' => $username,
		'password' => $password
	]);

	if($signin)
	{
		
        //Start the session
        session_start();

        //Dump your POST variables
        $_SESSION['username'] = $_POST['username'];
		header('Location: fareshare.php');
	}
}

?>


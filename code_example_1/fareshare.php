<?php
require_once 'app/init.php';
require_once 'app/session.php';
?>

  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Matthew Clarke">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />

    <title>FareShare</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">

  </head>

  <body onload="initialize()">
    <?php if($auth->check()): ?>


    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                  Handlebars Templates

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->


    <!-- ////////////   Journey Templates ///////////// -->

    <!-- Template to insert fuel + MPG -->
    <script id="fuel_tmpl" type="text/x-handlebars-template">

      {{#each objects}}
      <input type="hidden" name="mpg" class="mpg" value="{{mpg}}">
      <input type="hidden" name="fuel_cost" class="fuel_cost" value="{{fuel_cost}}"> {{/each}}

    </script>

    <!-- Template for map overview -->
    <script id="journey_overview_tmpl" type="text/x-handlebars-template">

      {{#each Overview}}

      <p>Distance : {{this.Distance}} Miles</p>
      <p>Cost Per Person : &pound; {{this.CostPerPerson}}</p>
      <p>Time : {{this.Time}}</p>

      {{/each}}

    </script>


    <!-- Template for Driver Modal -->
    <script id="driver_tmpl" type="text/x-handlebars-template">

      {{#each objects}}


      <div class="driver_id" title="{{friend_id}}" data-dismiss="modal">
        <div class="special">
          <img class="img-circle item" src="profile_img/{{profile_img}}" />
          <p> <span class="fname_display">{{first_name}}</span>
            <br />
            <span class="lname_display">{{last_name}}</span>
            <br />
            <img src="img/driver_sm.png" style="width:17px;" />
          </p>

        </div>


      </div>


      {{/each}}

    </script>


    <!-- Template for Passenger Modal -->
    <script id="passenger_add_tmpl" type="text/x-handlebars-template">

      {{#each objects}}


      <div class="passenger_id" title="{{friend_id}}">

        <img class="img-circle item" src="profile_img/{{profile_img}}" />

        <p> <span class="fname_display">{{first_name}}</span>
          <br />
          <span class="lname_display">{{last_name}}</span></p>

      </div>


      {{/each}}

    </script>



    <!-- ////////////   friends Templates  ///////////// -->

    <!-- TEMPLATE for find-result -->
    <script id="search_tmpl" type="text/x-handlebars-template">

      {{#each objects}}

      <div style="width:34%; float:left;">
        <img class="img-circle" style="horizontal-align: middle;" src="profile_img/{{profile_img}}" />
      </div>

      <div style="width:33%; float:left; position: relative;">

        <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, 50%)">
          <span class="fname_display">{{first_name}}</span>
          <br />
          <span class="lname_display">{{last_name}}</span>
        </div>
      </div>

      <div style="width:33%; float:left; position: relative;">
        <div style="position: absolute; top: 50%; left: 50%; transform: translate(275%, 200%)">
          <button class="add_friend btn btn-default btn-sm" type="submit" value="{{id}}">Add</button>
        </div>
      </div>


      {{/each}}

    </script>


    <!--Template for friends list-->
    <script id="friend_tmpl" type="text/x-handlebars-template">

      {{#each objects}}

      <div class="col-sm-6">

        <div>
          <img class="img-circle" src="profile_img/{{profile_img}}" />
        </div>

        <div class="name-fix">
          <span class="fname_display">{{first_name}}</span>
          <br />
          <span class="lname_display">{{last_name}}</span>
        </div>

        <div class="button-fix">
          <div class="dropdown">
            <button class="btn btn-default btn-sm dropdown-toggle" type="button" id="dropdownMenu1" style="float:right;" data-toggle="dropdown"
              aria-expanded="true">
                         {{#compare f_status 'friend' operator="=="}}
                          Friends
                          {{/compare}}
                          {{#compare f_status 'pending' operator="=="}}
                          Friend Request
                          {{/compare}}
                           {{#compare f_status 'waiting' operator="=="}}
                          Pending
                          {{/compare}}
                          <span class="caret"></span>
                      </button>

            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">


              {{#compare f_status 'friend' operator="=="}}
              <li role="presentation" value="{{friend_id}}"><a role="menuitem" tabindex="-1" href="#">Delete</a></li>
              {{/compare}} {{#compare f_status 'waiting' operator="=="}}
              <li role="presentation" value="{{friend_id}}"><a role="menuitem" tabindex="-1" href="#">Delete</a></li>
              {{/compare}} {{#compare f_status 'pending' operator="=="}}
              <li role="presentation" value="{{friend_id}}"><a role="menuitem" tabindex="-1" href="#">Accept</a></li>
              <li role="presentation" value="{{friend_id}}"><a role="menuitem" tabindex="-1" href="#">Decline</a></li>
              {{/compare}}

              <li role="presentation" value="{{friend_id}}"><a role="menuitem" tabindex="-1" href="#">Block</a></li>
            </ul>

          </div>
        </div>
      </div>

      {{/each}}
    </script>

    <!-- ///////////////////////////////////////////   Payment Templates ////////////////////////////////////////// -->

    <!-- TEMPLATE for verify journey -->
    <script id="verifyJourney_tmpl" type="text/x-handlebars-template">
      {{#if objects}}
      <li><b>{{CreatedOn}}</b>: You travelled with <b>{{first_name}} {{last_name}}</b> <button class='btn btn-sm btn-danger'
          style="float:right; margin-left: 5px;">X</button><button class='btn btn-sm btn-default' style="float:right;">Confirm</button></li>
      <hr /> {{else}}
      <p>No journeys to verify</p>
      {{/if}}
    </script>


    <script id="paymentOverview_tmpl" type="text/x-handlebars-template">
      {{#each objects}} {{#if overview_owe}}
      <p>You owe: &pound;{{overview_owe}} </p>
      {{else}}
      <p>You owe: &pound;0.00 </p>
      {{/if}} {{#if overview_due}}
      <p>You are owed: &pound;{{overview_due}} </p>
      {{else}}
      <p>You are owed: &pound;0.00 </p>
      {{/if}} {{#if no_journeys}}
      <p>You have been on {{no_journeys}} journeys</p>
      {{else}}
      <p>You have been on 0 journeys</p>
      {{/if}} {{/each}}
    </script>


    <script id="whatYouAreOwed_tmpl" type="text/x-handlebars-template">
      {{#each objects}} {{#if first_name}}
      <li><img class="img-circle" style="horizontal-align: middle;" src="profile_img/{{profile_img}}" />&nbsp;&nbsp;<span class="verify_text"> <b>{{first_name}} {{last_name}}</b> owes you <b>&pound;{{cost}}</b> from <b>{{journey_date}}</b></span>        <button title="{{payment_id}}" data-toggle="modal" data-target="#paymentVerifyModal" class='btn btn-sm btn-default whatYouAreOwed_verify'>Verify</button></li>
      {{else}}
      <li>No one currently owes you money</li>
      {{/if}} {{/each}}
    </script>

    <script id="whatYouOwe_tmpl" type="text/x-handlebars-template">
      {{#each objects}} {{#if first_name}}
      <li><img class="img-circle" style="horizontal-align: middle;" src="profile_img/{{profile_img}}" />&nbsp;&nbsp;<span>You owe <b>{{first_name}} {{last_name}} &pound;{{cost}}</b> from <b>{{journey_date}}</b></span></li>
      {{else}}
      <li>You currently owe no one money</li>
      {{/if}} {{/each}}
    </script>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Modal boxes

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    <!-- //////////////// payment verify modal ////////////////////////// -->


    <div class="modal fade" id="paymentVerifyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirm Payment</h4>
          </div>
          <div class="modal-body verifyText_input">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-default whatYouAreOwed2">Confirm Payment</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Main Nav

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    <div id="loading">

    </div>

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="helper" href="#">
            <img alt="Brand" id="brand" src="img/fs_logo.png" style="width:260px">
          </a>
          <ul class="main-menu">

            <li class="circle" id=".settings-section"><img src="img/settings_off.png" alt="settings" /></li>
            <li class="circle" id=".payment-section"><img src="img/payment_off.png" alt="payments" /></li>
            <li class="circle active" id=".journey-section"><img src="img/journey_on.png" alt="journeys" /></li>
            <li class="circle" id=".friends-section"><img src="img/friends_off.png" alt="friends" /></li>
          </ul>

          <span class="signout">Hello <?php echo $_SESSION['username']; ?> | <a href="signout.php">Sign out</a></span>



        </div>
      </div>
    </nav>
    <div class="ghost">
    </div>



    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Journey Section

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    <div class="journey-section">
      <noscript>
        <p class="bg-danger" style="color:#E03C48; text-align:center; margin-top:0; padding-bottom:15px; top:-10px; font-size:20px;">
          This application needs javascript to function properly. Please turn it on in your browser.
        </p>
      </noscript>

      <div class="row">

        <div class="col-sm-6">
          <div class="panel-head">
            <h1>Who is going?</h1>
          </div>
          <!--panel-head-->
          <div class="panel-body">
            <ul class="going-options">
              <li class="circle" data-toggle="modal" id="add_driver" data-target="#driverModal"><img src="img/driver_off.png" alt="" /><br />
                <p class="going-text">Driver</p>
              </li>
              <li class="circle" data-toggle="modal" id="add_passenger" data-target="#passengerAddModal"><img src="img/plus_off.png" alt="" /><br />
                <p class="going-text">Passengers</p>
              </li>
              <li class="circle" id="reset_car"><img src="img/reset.png" alt="" /><br />
                <p class="going-text">Reset</p>
              </li>
            </ul>

            <div class="journey-driver">

            </div>
            <div class="journey-passengers">

            </div>
          </div>
          <!--panel body-->



        </div>
        <!--clo-sm-6-->

        <div class="col-sm-6">
          <div class="panel-head">
            <h1>Route</h1>
          </div>
          <!--panel head-->


          <div class="panel-body">

            <form action="#" onsubmit="showLocation(); return false;">

              <div class="input-group" id="locationField">
                <span class="input-group-addon"><img src="img/point_a.png" alt="start point"/></span>
                <input type="text" class="form-control" name="origin1" id="origin1" placeholder="Start Point">
              </div>
              <!--input-group-->

              <div class="input-group">
                <span class="input-group-addon"><img src="img/point_b.png" alt="End point" /></span>
                <input type="text" class="form-control" id="destinationa" name="destination1" placeholder="Finish Point">
              </div>
              <!--input-group-->
              <br /><br />
              <div class="btn-group single-return" role="group" style="margin-left: 5%;">
                <button type="button" class="btn btn-default selected">Single</button>
                <button type="button" class="btn btn-default">Return</button>
              </div>
              <!--btn group-->


              <div id="hidden_mpg_fuel">
              </div>
              <input type="hidden" name="passengers" class="passengers" value="">

              <button type="button" disabled='true' onclick="calculateDistances();" class="btn btn-default view_map" style="float:right; margin-right: 5%;">Calculate</button>
            </form>
          </div>
          <!--panel body-->
        </div>
        <!--clo-sm-6-->
      </div>
      <!--row-->


      <div class="row">

        <div class="col-sm-12">
          <div id="map-canvas"></div>
          <!--map canvas-->

          <div id="map-overlay">


            <div id="outputDiv2"></div>






          </div>
          <!--map overlay-->



        </div>
        <!--clo-sm-12-->

        <div class="col-sm-6">
          <div class="panel-body">
            <form id="hidden-journey">
              <input type="hidden" name="driver" id="driver" value="">
              <input type="hidden" name="p1" id="p1" value="">
              <input type="hidden" name="p2" id="p2" value="">
              <input type="hidden" name="p3" id="p3" value="">
              <input type="hidden" name="p4" id="p4" value="">
              <input type="hidden" name="distance" id="distance" value="">
              <input type="hidden" name="cost" id="cost" value="">

              <p>If you are happy with your journey then please submit</p>

              <button type="button" class="btn btn-lg btn-default submit_journey" style="float:right; margin-right: 5%;">Submit</button>
            </form>
          </div>
        </div>

      </div>
      <!--row-->


      <!-- //////////////////////////////// Driver Modal /////////////////////////////////////// -->
      <!-- Button trigger modal -->

      <!-- Modal -->
      <div class="modal fade" id="driverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Chose a Driver</h4>
            </div>


            <div class="modal-body">
              <div class="row" id="driver_list">
              </div>
            </div>

          </div>
        </div>
      </div>


      <!-- //////////////////////////////// Passenger Modal /////////////////////////////////////// -->
      <!-- Button trigger modal -->

      <!-- Modal -->
      <div class="modal fade" id="passengerAddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Chose up to 4 passengers</h4>
            </div>




            <div class="modal-body">
              <div class="row" id="passenger_add_list">
              </div>
              <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">Going</h4>
              </div>
              <div class="modal-footer passenger-footer">

              </div>
            </div>

          </div>
        </div>
      </div>


    </div>
    <!--journey section-->


    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Friends Section

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
    <div class="friends-section">
      <div class="blue-bg">
        <div class="row">
          <div class="col-sm-6">

            <div class="panel-head">
              <h1>Add a Friend</h1>
            </div>
            <!--panel-head-->

            <div class="panel-body">
              <div class="input-group">
                <span class="input-group-addon">
                  <img src="img/point_b.png" alt="" />
                </span>
                <input type="text" id="search_friend" class="form-control" placeholder="Username">
              </div>
              <!--input-group-->
              <br /><br />
              <button type="button" id="find_friend" class="btn btn-default" style="float:right; margin-right: 5%;">Search</button>
            </div>
            <!--panel-body-->

          </div>
          <!--clo-sm-6-->



          <div class="col-sm-6">




            <div class="panel-body" id="find_result" style=" border-top-left-radius: 10px;">
            </div>
            <!--panel-body-->

          </div>
          <!--clo-sm-6-->
        </div>
        <!--row-->
      </div>
      <!--blue-bg-->



      <div class="row">

        <div class="col-sm-12">
          <div class="panel-head">
            <h1>Friends</h1>
          </div>
          <!--panel-head-->
          <div class="panel-body">




            <div id="friends_list">
            </div>
            <!--friends_list-->


          </div>
          <!--panel head-->
        </div>
        <!--col-sm-12-->
      </div>
      <!--row-->





    </div>
    <!--Friends-section-->


    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Payment Section

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->




    <div class="payment-section">

      <div class="blue-bg">
        <div class="row">

          <div class="col-sm-6">
            <div class="panel-head">
              <h1>Overview</h1>
            </div>
            <!--panel-head-->
            <div class="panel-body">
              <div class="payment_overview">
              </div>

              <br /><br />
              <button type="button" data-toggle="modal" data-target="#paymentHistoryModal" id="payment_history" class="btn btn-default"
                style="float:left; margin-right: 5%;">Payment History</button>
              <button type="button" class="btn btn-default" style="float:right; margin-right: 5%;">Journey History</button>
            </div>
            <!--panel-body-->
          </div>
          <!--clo-sm-6-->

          <div class="col-sm-6">
            <div class="panel-head">
              <h1>Verify Journeys</h1>
            </div>
            <!--panel-head-->

            <div class="panel-body">
              <ul id="verifyJourney-input">
              </ul>

            </div>
            <!--panel-body-->

          </div>
          <!--clo-sm-6-->
        </div>
        <!--row-->

      </div>
      <!--blue-bg-->

      <div class="row">

        <div class="col-sm-6">
          <div class="panel-head">
            <h1>Who You Owe</h1>
          </div>
          <!--panel-head-->
          <div class="panel-body">
            <ul class="whatYouOwe">
            </ul>

          </div>
          <!--panel-body-->
        </div>
        <!--clo-sm-6-->

        <div class="col-sm-6">
          <div class="panel-head">
            <h1>What You Are Owed</h1>
          </div>
          <!--panel-head-->
          <div class="panel-body">
            <ul class="whatYouAreOwed">
            </ul>
          </div>
          <!--panel-body-->

        </div>
        <!--clo-sm-6-->
      </div>
      <!--row-->



    </div>
    <!--payment-section-->



    <!-- //////////////////////////////// payment history modal /////////////////////////////////////// -->


    <!-- Modal -->
    <div class="modal fade" id="paymentHistoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Payment History</h4>
          </div>




          <div class="modal-body">
            <div class="row" id="paymentHistoryInfo">
            </div>
          </div>

        </div>
      </div>
    </div>




    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////

                                                    Settings Section

         ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->




    <div class="settings-section">

      <div class="row">

        <div class="col-sm-12">
          <div class="panel-head">
            <h1>Settings</h1>
          </div>
          <!--panel-head-->
          <div class="panel-body">


            <div class="col-sm-6">
              <div class="blue-border">
                <h3><b>Upload Profile Pic</b></h3>

                <img class="img-circle" src="profile_img/default.jpg" />


                <form action="app/upload.php" method="post" enctype="multipart/form-data">

                  <input type="file" class="form-control" name="fileToUpload" id="fileToUpload">
                  <p class="help-block"> 200x200 Recommended size</p>
                  <input type="submit" disabled="true" class="btn btn-default btn-sm" id="upload_btn" value="Upload Image" name="submit">
                </form>
              </div>
            </div>






            <div class="row">
              <div class="col-sm-6">
                <div class="blue-border">
                  <h3><b>Edit Car Details</b></h3>

                  <h4>Your Cars Miles Per Gallon (MPG)</h4>
                  <div class="input-group">
                    <input type="number" class="form-control" id="mpg_input" placeholder="e.g. 43">
                    <div class="input-group-addon">Miles Per Gallon (MPG)</div>
                  </div>

                  <script id="fuelPrices_tmpl" type="text/x-handlebars-template">
                    <h4>Enter Fuel Cost (£/L)</h4>

                    {{#each objects.Fuels}}
                    <div class="radio-inline">
                      <label><input type="radio" name="optradio" value="" checked>Custom</label>
                    </div>
                    {{#if Unleaded}}
                    <div class="radio-inline">
                      <label><input type="radio" name="optradio" value="{{Unleaded}}">UK Unleaded average</label>
                    </div>
                    {{/if}} {{#if Diesel}}
                    <div class="radio-inline">
                      <label><input type="radio" name="optradio" value="{{Diesel}}">UK Diesel average</label>
                    </div>
                    {{/if}}


                    <div class="input-group">
                      <input type="number" class="form-control" id="fuel_price" placeholder="e.g. 108.6">
                      <div class="input-group-addon">pence per litre</div>
                    </div>
                    <button class="btn btn-default update-car" type='button'>Update</button> {{/each}}
                  </script>


                  <div id="fuel_radio"></div>
                </div>
              </div>
              <!--blue-border-->





              <script id="settings_tmpl" type="text/x-handlebars-template">

                {{#each objects}}




                <div class="form-group">
                  <h4>Username</h4>
                  <input type="text" class="form-control" value="{{username}}">
                </div>
                <!--form-group-->

                <div class="form-group">
                  <h4>First Name</h4>
                  <input type="text" pattern="[A-Za-z]{50}" class="form-control" value="{{first_name}}">
                </div>
                <!--form-group-->

                <div class="form-group">
                  <h4>Last Name</h4>
                  <input type="text" pattern="[A-Za-z]{50}" class="form-control" value="{{last_name}}">
                </div>
                <!--form-group-->

                <div class="form-group">
                  <h4>Email</h4>
                  <input type="email" class="form-control" value="{{email}}">
                </div>
                <!--form-group-->

                <button class="btn btn-default update-settings" type='button'>Update</button> {{/each}}

              </script>
              <div class="col-sm-6">
                <div class="blue-border gap">
                  <h3><b>Edit Personal Details</b></h3>
                  <form class="form" id="settings-input">
                  </form>
                </div>
                <!--blue-border-->
              </div>
              <!--col-sm-6-->
            </div>
            <!--row-->


          </div>
          <!--panel-body-->
        </div>
        <!--clo-sm-12-->

      </div>
      <!--row-->
    </div>
    <!--settings-section-->










    <footer class="footer">
      <div class="container">
        <ul class="main-menu">

          <li class="circle" id=".settings-section"><img src="img/settings_off.png" alt="settings" /></li>
          <li class="circle" id=".payment-section"><img src="img/payment_off.png" alt="payments" /></li>
          <li class="circle active" id=".journey-section"><img src="img/journey_on.png" alt="journeys" /></li>
          <li class="circle" id=".friends-section"><img src="img/friends_off.png" alt="friends" /></li>
        </ul>
      </div>
    </footer>


    <?php else: ?>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="helper" href="#">
            <img alt="Brand" src="img/fs_logo.png">
          </a>




        </div>
      </div>
    </nav>
    <div class="col-sm-3">
    </div>
    <div class="col-sm-6">
      <div class="panel-head">
        <h1>Please sign in</h1>
      </div>
      <!--panel-head-->
      <div class="panel-body">
        <form class="form-signin" method="post" action="signin.php">

          <br />
          <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
          <br />
          <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
          <br />
          <a href="index.php">Register here</a><button class="btn btn-lg btn-default" type="submit">Sign in</button>
        </form>
      </div>
      <!--panel-body-->

    </div>
    <!-- sm-6-->
    <div class="col-sm-3">
    </div>




    <?php endif; ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/handlebars-v3.0.0.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="js/google_maps.js"></script>
    <script src="js/main.js"></script>


    <script>
      Handlebars.registerHelper('compare', function (lvalue, rvalue, options) {

        if (arguments.length < 3)
          throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

        operator = options.hash.operator || "==";

        var operators = {
          '==': function (l, r) {
            return l == r;
          },
          '===': function (l, r) {
            return l === r;
          },
          '!=': function (l, r) {
            return l != r;
          },
          '<': function (l, r) {
            return l < r;
          },
          '>': function (l, r) {
            return l > r;
          },
          '<=': function (l, r) {
            return l <= r;
          },
          '>=': function (l, r) {
            return l >= r;
          },
          'typeof': function (l, r) {
            return typeof l == r;
          }
        }

        if (!operators[operator])
          throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);

        var result = operators[operator](lvalue, rvalue);

        if (result) {
          return options.fn(this);
        } else {
          return options.inverse(this);
        }

      });
    </script>

    <!-- <script src="js/docs.min.js"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="js/ie10-viewport-bug-workaround.js"></script>-->
  </body>

  </html>
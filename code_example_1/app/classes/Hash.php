<?php

class Hash
{
	
    //this function will hash a password
	public function make($plain)
	{
		return password_hash($plain, PASSWORD_BCRYPT, ['cost' => 10]);
	}

	//this function will verify a password
	public function verify($plain, $hashed)
	{
		return password_verify($plain, $hashed);
	}
}
<?php

//retrieve petrol prices xml from URL
$xml=simplexml_load_file('http://www.petrolprices.com/feeds/averages.xml') 
     or die("Error: Cannot create object");

//select unleaded and diesel from xml
$avgUnleaded = $xml->Fuel[1]->Average;
$avgDiesel = $xml->Fuel[4]->Average;

//create a JSON string with the two prices
$string = '{"Fuels":[
    {"Unleaded":"'.$avgUnleaded.'"}, 
    {"Diesel":"'.$avgDiesel.'"}
]}';

echo $string;
?>
<?php
require_once 'init.php';


$username=$_SESSION['username'];
$id=$_SESSION['id'];
$query_id = "Select id From users where username='".$username."'";

//connect to database
//require_once('database.php');

// If user isn't login they can't access information 
// from directly accessing this page
if($auth->check()){


/////////////////////////////////////////// Get friends List


if(!isset($_POST['search_username']) && !isset($_POST['type']) && !isset($_POST['value']) ){

	

try {
    $results = $db_connect->query(
      "select friends.friend_id, friends.f_status, users.profile_img, users.first_name, users.last_name from friends 
      INNER JOIN users
      ON friends.friend_id=users.id
      where (friends.f_status='friend' or friends.f_status='pending' or friends.f_status='waiting')  AND friends.user_id = ($query_id)
      ORDER BY friends.f_status DESC, users.last_name ASC"
      );
} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$friends = $results->fetchAll(PDO::FETCH_ASSOC);
$friends = json_encode($friends);

echo $friends;



}



/////////////////////////////////////////// Find a person via username

 else if(isset($_POST['search_username']) && !isset($_POST['type']) && !isset($_POST['value']) ){
  
  $search_username = $_POST['search_username'];
 


  /*select users.id, users.profile_img, users.first_name, users.last_name from users 
      LEFT JOIN friends
      ON friends.friend_id=users.id
      where (friends.f_status is null AND friends.user_id is null AND friends.user_id <> '".$id."' )
      
      AND users.username =  '".$search_username."'
       AND users.username <>  '".$username."' */



try {
    $results = $db_connect->query("
      SELECT users.id, users.profile_img, users.first_name, users.last_name 
      FROM users 
      LEFT JOIN friends
      ON friends.friend_id=users.id
      where (users.id != '".$id."' AND friends.user_id is null AND friends.friend_id is null)
      AND users.username = '".$search_username."'
  
     ");
} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$friends = $results->fetchAll(PDO::FETCH_ASSOC);
$friends = json_encode($friends);

echo $friends;




}



/////////////////////////////////////////// Accept, Decline, Delete and block a friend

else if(isset($_POST['type']) && isset($_POST['value']) ){
	$FriendID = $_POST['value'];
    $type = $_POST['type'];



if ($type == 'Delete' || $type == 'Decline' ){

try {
    $results = $db_connect->query("DELETE FROM friends WHERE (user_id = ($query_id) AND friend_id = $FriendID) OR (friend_id = ($query_id) AND user_id = $FriendID)");
} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$results->execute();

}

else if ($type == 'Accept'){


try {
    $results = $db_connect->query("UPDATE friends
SET f_status='friend'
WHERE (user_id = ($query_id) AND friend_id = '".$FriendID."')  OR (friend_id = ($query_id) AND user_id = '".$FriendID."')");
} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$results->execute();

}

else if ($type == 'Block'){


try {
    $results = $db_connect->query("UPDATE friends
SET f_status='blocked'
WHERE (user_id = ($query_id) AND friend_id = '".$FriendID."')  OR (friend_id = ($query_id) AND user_id = '".$FriendID."')");
} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$results->execute();

}

}

/////////////////////////////////////////// add a friend (Will be pending)

if(isset($_POST['friendID'])){
	

  $FriendID = $_POST['friendID'];
   


try {
    $results = $db_connect->query("
      INSERT INTO friends
   VALUES
  (($query_id), $FriendID, 'waiting'),
  ($FriendID, ($query_id), 'pending')

      ");

    	} 
catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$results->execute();

}



}
?>

<?php
require_once 'init.php';

$username=$_SESSION['username'];
$id= $_SESSION['id'];

// If user isn't login they can't access information 
// from directly accessing this page
if($auth->check()){


/////////////////////////////////////////// Get friends List


if(isset($_POST['verify_journeys'])){

	

try {
    $results = $db_connect->query("select journey.journey_id, journey.driver, journey.cost, journey.CreatedOn, users.first_name, users.last_name FROM journey
      INNER JOIN users
      ON journey.driver=users.id

     
      where (journey.driver = '".$id."' AND journey.d_status = 'no')
       OR (journey.p1 = '".$id."' AND journey.p1_status = 'no')
       OR (journey.p2 = '".$id."' AND journey.p2_status = 'no')
       OR (journey.p3 = '".$id."' AND journey.p3_status = 'no')
       OR (journey.p4 = '".$id."' AND journey.p4_status = 'no')
    
      ORDER BY journey.CreatedOn");
} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$friends = $results->fetchAll(PDO::FETCH_ASSOC);
$friends = json_encode($friends);

echo $friends;
}




if(isset($_POST['payment_overview'])){

  

try {
    $results = $db_connect->query("
    SELECT(
    SELECT SUM(cost) FROM payment WHERE giver = ".$id." AND p_status = 'no'
    ) AS overview_owe,
    (
    SELECT SUM(cost) FROM payment WHERE receiver = ".$id." AND p_status = 'no'
    ) AS overview_due,
    (
    SELECT COUNT(*) FROM payment WHERE receiver = ".$id." OR giver = ".$id."
    ) AS no_journeys

    ");

} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$data = $results->fetchAll(PDO::FETCH_ASSOC);
$data = json_encode($data);

echo $data;
}





if(isset($_POST['whatYouAreOwed'])){


try {
    $results = $db_connect->query("
    
     select payment.journey_date, payment.payment_id, payment.cost, users.first_name, users.last_name, users.profile_img FROM payment
      INNER JOIN users
      ON payment.giver=users.id
      where payment.receiver = ".$id."
      AND payment.p_status = 'no'
     
    ");

} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$data = $results->fetchAll(PDO::FETCH_ASSOC);
$data = json_encode($data);

echo $data;

}



if(isset($_POST['whatYouOwe'])){


try {
    $results = $db_connect->query("
    
     select payment.journey_date, payment.payment_id, payment.cost, users.first_name, users.last_name, users.profile_img FROM payment
      INNER JOIN users
      ON payment.receiver=users.id
      where payment.giver = ".$id."
      AND payment.p_status = 'no'
     
    ");

} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$data = $results->fetchAll(PDO::FETCH_ASSOC);
$data = json_encode($data);

echo $data;

}



if(isset($_POST['payment_history'])){


try {
    $results = $db_connect->query("
    
     select * FROM payment
      INNER JOIN users
      ON payment.receiver=users.id
      AND payment.giver=users.id
      where payment.giver = ".$id."
      OR payment.receiver = ".$id."
      
     
    ");

} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$data = $results->fetchAll(PDO::FETCH_ASSOC);
$data = json_encode($data);

echo $data;

}


//confirm payment
if(isset($_POST['journey_id'])){

$journey_id = $_POST['journey_id'];

try {
    $results = $db_connect->query("
    

    UPDATE payment SET p_status='yes'
    WHERE payment_id = ".$journey_id."
     
 
    ");

  $results->execute();



} catch(Exception $e) {
    echo $e->getMessage();
    die();
}

$data = $results->fetchAll(PDO::FETCH_ASSOC);
$data = json_encode($data);

echo $data;

}




}




?>


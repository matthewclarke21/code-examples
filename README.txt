

Code example 1
____________________________

University final year project called FareShare. An online application that allows users to work out the cost of a journey when they are carpooling with their friends. (Note: Since release the Google maps API has recently changed and therefore the application is not operational without making amends.)


Code example 2
____________________________

A WordPress Site created for a client while working at The Design Factor. You can visit the live website here: https://scintacor.com
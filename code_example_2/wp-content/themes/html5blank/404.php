<?php get_header(); ?>

<div class="container search-page">
 
	<div class="row">
		<div class="col-sm-9 col-product">
         <h1>Sorry this page does not exist</h1>
         <p>Return to the <a href="<?php echo get_site_url(); ?>">homepage</a></p>
		</div>
		<div class="col-sm-3 col-product-sidebar">
      <?php dynamic_sidebar( 'widget-area-1' ); ?>
		</div>
	</div>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
</div><!--/container-->

<?php get_footer(); ?>

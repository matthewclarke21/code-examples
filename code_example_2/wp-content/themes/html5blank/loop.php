<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- post title -->
		<h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		</h2>
		<!-- /post title -->

		<?php 
        $info = html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php 
		echo $info;
		
        if (strlen($info)<20){
        $other_page = get_the_ID();
        $info = get_field('product_information' , $other_page);
        $info = substr($info, 0, 142);
        echo $info;
        }
		
		?>

	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>

<?php /* Template Name: Markets Template */ get_header(); ?>

<div class="container">
    <?php if(post_password_required()): ?>
       <?php  echo get_the_password_form(); ?>
     <?php else: ?>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
   <div class="row">
    <div class="feature">
     <div class="feature-image" style="background-image: url(<?php the_field('feature_image'); ?>);"></div>
      <?php

         if(get_field('feature_text'))
          {
           echo '<h1>' . get_field('feature_text') . '</h1>';
          }

        if(get_field('feature_sub-text'))
          {
           echo '<p>' . get_field('feature_sub-text') . '</p>';
          }

       ?>
        <?php custom_breadcrumbs(); ?>
    </div><!--feature-image-->
   </div>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
	<div class="row row-gap row-markets">
		<div class="col-sm-3">
	   <?php the_field('column_1'); ?>
		</div>
		<div class="col-sm-3">
	   <?php the_field('column_2'); ?>
		</div>
		<div class="col-sm-3">
	   <?php the_field('column_3'); ?>
		</div>
		<div class="col-sm-3">
	   <?php the_field('column_4'); ?>
		</div>
	</div>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
<?php endif ?>
</div><!--/container-->


<?php get_footer(); ?>
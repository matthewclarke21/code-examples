<?php /* Template Name: Homepage Template */ get_header(); ?>

<div class="container">
  <?php if(post_password_required()): ?>
       <?php  echo get_the_password_form(); ?>
     <?php else: ?>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
   <div class="row">
    <div class="feature">
     <div class="feature-image" style="background-image: url(<?php the_field('feature_image'); ?>);"></div>
      <?php

         if(get_field('feature_text'))
          {
           echo '<h1>' . get_field('feature_text') . '</h1>';
          }

        if(get_field('feature_sub-text'))
          {
           echo '<p>' . get_field('feature_sub-text') . '</p>';
          }

       ?>
    </div><!--feature-image-->
   </div>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
	<div class="row row-gap">
		<div class="col-sm-3">
		<?wp_nav_menu( array( 'theme_location' => 'homepage-markets', 'menu_class' => 'homepage-menu' ) ); ?>
		</div>
		<div class="col-sm-3">
		<?wp_nav_menu( array( 'theme_location' => 'homepage-capabilities', 'menu_class' => 'homepage-menu' ) ); ?>
		</div>
		<div class="col-sm-3">
		<?wp_nav_menu( array( 'theme_location' => 'homepage-technologies', 'menu_class' => 'homepage-menu' ) ); ?>
		</div>
		<div class="col-sm-3">
		<p class="statement">customised excellence at the core of your system</p>
		</div>
	</div>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  

    <div class="row">
      <div class="col-sm-6">
         <h3 class="purple-heading">
          <?php
             if(get_field('toggle_event') == 'no')
               {
                echo 'Recent Blog Posts';
               }

             else{
                echo "Recent Blog Post";
               }
            ?>
          </h3>
            <ul class="recent-blog-post">
               <!-- Define our WP Query Parameters -->
               <?php $the_query = new WP_Query( 'posts_per_page=1' ); ?>

               <!-- Start our WP Query -->
               <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

               <!-- Display the Post Title with Hyperlink -->
               <li><h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4></li>

               <!-- Display the Post Excerpt -->
               <li><?php the_excerpt(); ?></li>

               <!-- Repeat the process and reset once it hits the limit -->
			   <?php 
			   endwhile;
			   wp_reset_postdata();
			   ?>
            </ul>
     </div>

     <div class="col-sm-6">
     	 

       <!--  ++++++++++++++++++++++++ Blog Post +++++++++++++++++++++++++++  -->
       <h3 class="purple-heading">
         <?php
             if(get_field('toggle_event') == 'no')
               {
                echo '';
               }

             else{
                echo "Upcoming Event";
               }
          ?>
       </h3>
     

     <?php if(get_field('toggle_event') == 'no') : ?>
             

        <ul class="recent-blog-post">
               <!-- Define our WP Query Parameters -->
               <?php $the_query = new WP_Query( 'posts_per_page=1&offset=1' ); ?>

               <!-- Start our WP Query -->
               <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

               <!-- Display the Post Title with Hyperlink -->
               <li><h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4></li>

               <!-- Display the Post Excerpt -->
               <li><?php the_excerpt(); ?></li>

               <!-- Repeat the process and reset once it hits the limit -->
			   <?php 
			   endwhile;
			   wp_reset_postdata();
			   ?>
        
            </ul>

          <?php endif; ?>
        <!-- / ++++++++++++++++++++++++ Blog Post +++++++++++++++++++++++++++  -->


       <?php if(get_field('toggle_event') == 'yes') : ?>
             

        <div class="recent-blog-post recent-blog-post-event">
             <div class="rbp-left">
               <a href="<?php the_field('url'); ?>"><img alt="latest event" src="<?php the_field('image'); ?>" /></a>
             </div>
             <div class="rbp-right">
              <p><?php the_field('date'); ?></p>
              <a href="<?php the_field('url'); ?>" target="_blank"><?php the_field('name'); ?></a>
              <p><?php the_field('location'); ?></p>
             </div>
            
                   <a class="all-events" href="<?php echo home_url(); ?>/news">All Events</a>
            </div>

          <?php endif; ?>


<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --> 

        

     </div>

    </div>

<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --> 
<?php endif ?>
</div><!--/container-->


<?php get_footer(); ?>
<!-- search -->
<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input" type="search" name="s" placeholder=" Search Scintacor">
	<button class="search-submit" type="submit"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/search.svg" alt="search" /></button>
</form>
<!-- /search -->

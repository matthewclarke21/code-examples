<?php get_header(); ?>

<div class="container search-page">
 
	<div class="row">
		<div class="col-sm-9 col-product">
         <h1><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<?php get_template_part('loop'); ?>
			<?php get_template_part('pagination'); ?>

			
		</div>
		<div class="col-sm-3 col-product-sidebar">
      <?php dynamic_sidebar( 'widget-area-1' ); ?>
		</div>
	</div>
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
</div><!--/container-->

<?php get_footer(); ?>

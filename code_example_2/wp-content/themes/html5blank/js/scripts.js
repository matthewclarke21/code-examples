(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		   // ----------------------------------------------------------------------------- Mobile Menu
            
            var is_open = 'no';

            $('.hamburger').on('click', function () {
                
            	$(this).toggleClass('is-active');
            	
            	if (is_open == 'no'){
            	  $('.mobile-nav').css("display", "flex").hide().fadeIn();
                  is_open = 'yes';              
                }

                else if (is_open == 'yes'){
                   $('.mobile-nav').css("display", "flex").show().fadeOut();
                   is_open = 'no';   
                }

         });


// Reset Mobile menu 
$(window).on('resize', function(){
        is_open = 'no';
        $('.mobile-nav').fadeOut();
        $('.hamburger').removeClass('is-active');
});
 
     // ------------------------------------------------------------------------------- /Mobile Menu

		
	});
	
})(jQuery, this);

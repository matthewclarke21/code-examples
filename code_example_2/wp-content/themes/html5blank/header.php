<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

	<div class="header">
     <div class="container flex-header">
        <a class="logo" href="<?php echo home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/img/scintacor_logo.jpg" alt="Logo" class="logo-img">
		</a>

		<nav class="">
            <?php get_search_form(); ?>
         
            <?php html5blank_nav(); ?>
          
        </nav>


        <!-- Mobile Menu -->


        <!-- END Mobile Menu -->

     </div><!--container-->
	</div><!--header-->

        <button class="mobile-menu-btn hamburger hamburger--3dx" type="button">
          <span class="hamburger-box">
          <span class="hamburger-inner"></span>
          </span>
        </button>
	
   
        <div class="mobile-nav">
          <?php html5blank_nav(); ?>
          <?php get_search_form(); ?>
        </div>	
	
           






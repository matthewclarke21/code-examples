<?php get_header(); ?>

<div class="container">
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
   <div class="row">
    <div class="feature">
     <div class="feature-image" style="background-image: url(<?php echo home_url(); ?>/wp-content/uploads/2015/05/fibre-optic.jpg);"></div>
     <h1>flexibility to deliver clever<br />and custom scintillation<br /> components</h1>
     <p>put brighter scintillation at the core of your system</p>
    </div><!--/feature-image-->
   </div><!--/row-->
 <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->  
  <div class="row">
    <div class="col-sm-9 main-content">
      <section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post title -->
			<h1>
			<?php the_title(); ?>
			</h1>
			<!-- /post title -->

			<?php the_content(); // Dynamic Content ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	<?php get_template_part('pagination'); ?>

	</section>
	<!-- /section -->
    </div><!--/col-sm-9-->

    <div class="col-sm-3 blog-sidebar">
     <?php dynamic_sidebar( 'widget-area-1' ); ?>
    </div>
  </div><!--/row-->

</div><!--/container-->

<?php get_footer(); ?>

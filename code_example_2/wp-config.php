<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache




/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
 //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/vhost/vhost15/s/c/i/scintacor.com/www/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'scintacor');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'prCp[PXmu-cZZn+c/QU=>%@lPNbgOG$S_a(E^%WUFjFW@HB-kqs%dsYIujhLmb|Un<Fz_V+sMP[lU/WMojmo?uT?(|fp[T@P=CT?&OV+orGS?+g@vU|hwwQEd;eE}-(@');
define('SECURE_AUTH_KEY', '}HxBxvIxnkE^?LC}hIt&fLBXI([A_$mhSCq|nd}(TBz^ZwNyy()RnmfNBS}}^QITfY;!)PcitAexB%X)[ghjA(C|=z&cqy{I|}Q<p|@RykE[ER[@q$OQ=;wUSG{G>!_N');
define('LOGGED_IN_KEY', '@DcSj+w{(HCHGnhuWDCtVbUuTULPKd+O;hi{nG&>^b)j*d&%_+M[{DUX!;)o=a-s)/}fqZ)ELAQi=yIF!Z)=}LcbqJq]PD>kVvHu>D*K<*RG]XlNFjLGAGDXC|Xkj;pb');
define('NONCE_KEY', 't%;z+[WFE$UW_@OgT=Kv+FRwVA_lpuN/m_??_NpI/Zi_RnOxOfa>=%|t({J{Ghg|c*/GhCE$X[wIRAYDiTTSbqGChx-$HcEJ_+|)?;R+-D{gauhtbqahW}+X-?[Y<*vf');
define('AUTH_SALT', '@MR<zCkeXUE{(G?$D<cs?%iJPvYyiC>zb_/_Z^(<[ItbEcEbPo/Gfb(NyT(ntU?)JC+dzccw)B$Uc[n@$cyWWL^yt%CMs)CdI|{aq@adzCmwob(/^ck)(IacJch<rm+&');
define('SECURE_AUTH_SALT', 'a+SMMBdL!%VaRUM;UsPBRFNh=f|hM^gVdUCrVgr@@k|iNu]-hsZLHjaAShBNa|qPcSl@iL%aKEShlUCw;oqgTGxegxsDIQQ^wAnC<=wEaAj==%$h;P_W[TP(PphkhNLj');
define('LOGGED_IN_SALT', 'VNG(ajbuPGqgfc*EgBn?rzI)>!Vj@DdmVtTU&FLa<Yta&ecnAyY|asx=MC(luqf?|RvhIheXs-</Yx(;y?ACyO_Iyro/uGdq</LE%%-(d]QJ[W-{uY&rzl}lUrcKI%HH');
define('NONCE_SALT', 'NB^qM(%xh[zS/QvXmQWx|dTG;HiK|QXY]wXmQ&xYf$pT([ywD&k&nwx<>h@[SD?zewws!SCX%eXn$]_)yERewpA;AREzXy?pkHNnY]Z};HyPq|$A+hf;MBCEciclD!Yi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_lwlp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Include tweaks requested by hosting providers.  You can safely
 * remove either the file or comment out the lines below to get
 * to a vanilla state.
 */
if (file_exists(ABSPATH . 'hosting_provider_filters.php')) {
	include('hosting_provider_filters.php');
}
